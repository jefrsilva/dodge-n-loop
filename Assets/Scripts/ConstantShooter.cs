﻿using UnityEngine;
using System.Collections;

public class ConstantShooter : MonoBehaviour {

	[Range (0.1f, 3.0f)]
	public float shootingInterval;
 	public GameObject bullet;

	private float elapsedTime;

	void Start () {
		elapsedTime = 0.0f;
	}
	
	void Update () {
		elapsedTime += Time.deltaTime;
		while (elapsedTime > shootingInterval) {
			elapsedTime -= shootingInterval;
			Instantiate (bullet, transform.position, Quaternion.identity);
		}
	}
}
