﻿using UnityEngine;
using System.Collections;

public class LoopHandler : MonoBehaviour
{
	public GameObject deathFX;

	private Vector3 lastPosition;
	private float cumulativeAngle;
	private Player player;

	void Start ()
	{
		GameObject obj = GameObject.FindGameObjectWithTag ("Player");
		if (obj != null) {
			player = obj.GetComponent<Player> ();			
		}
	}

	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player") && player.isLooping ()) {
			lastPosition = other.transform.position - transform.position;
			cumulativeAngle = 0.0f;
		}
	}

	void OnTriggerStay2D (Collider2D other)
	{
		if (other.CompareTag ("Player") && player.isLooping ()) {
			Vector3 currentPosition = other.transform.position - transform.position;

			float angle = Vector3.Angle (lastPosition, currentPosition) * Mathf.Sign (Vector3.Cross (currentPosition, lastPosition).z);
			transform.Rotate (new Vector3 (0.0f, 0.0f, -angle));

			cumulativeAngle += angle;

			if (Mathf.Abs (cumulativeAngle) >= 360.0f) {
				player.notifyDestroy ();
				Instantiate (deathFX, transform.position, Quaternion.identity);
				Destroy (this.gameObject);
			}
			lastPosition = currentPosition;
		}
	}

}
