﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Text;

public class GameController : MonoBehaviour {

	private int score;
	private int dodges;
	private float loopTime;
	private int remainingScoreToAdd;
	public Text scoreText;
	public Text actionText;

	void Start () {
		score = 0;
		dodges = 0;
		loopTime = 0.0f;
		StartCoroutine ("UpdateScore");
	}
	
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {	
			Application.Quit ();
		}
	}

	public void EndGame() {
		StopCoroutine ("UpdateScore");
		StartCoroutine ("RestartGame");
	}

	public void addScore(int score) {
		remainingScoreToAdd += score;
	}

	public void notifyDodge() {
		dodges++;
	}

	public void notifyLoop(float loopTime) {
		this.loopTime = loopTime;
	}

	IEnumerator RestartGame() {
		yield return new WaitForSeconds (5f);
		SceneManager.LoadScene (0);
	}

	IEnumerator UpdateScore() {
		while (true) {
			if (remainingScoreToAdd >= 50) {
				score += 50;
				remainingScoreToAdd -= 50;
				scoreText.fontSize = 18;
			} else {
				scoreText.fontSize = 14;
			}
			scoreText.text = string.Format ("{0}", score);

			if (loopTime <= 0.0f) {
				actionText.fontSize = 14;
				actionText.color = Color.black;
				actionText.text = string.Format ("Dodge {0} more bullets to LOOP!", 5 - dodges);
			} else {
				dodges = 0;
				loopTime -= 0.05f;
				if (loopTime < 0.0f) {
					loopTime = 0.0f;
				}
				actionText.fontSize = 18;
				actionText.color = Color.red;
				string loop = "L";
				for (int i = 0; i < loopTime * 5; i++) {
					loop += "o";
				}
				loop += "p!";
				actionText.text = loop;
			}

			yield return new WaitForSeconds (0.05f);			
		}
	}
}
