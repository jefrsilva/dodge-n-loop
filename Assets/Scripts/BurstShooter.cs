﻿using UnityEngine;
using System.Collections;

public class BurstShooter : MonoBehaviour {

	[Range (1.0f, 10.0f)]
	public float intervalBetweenBursts;
	[Range (0.1f, 3.0f)]
	public float burstShootingInterval;
	public int burstLength; 

	public GameObject bullet;

	private float elapsedTime;
	private int currentBurst;
	private bool inBurst;

	void Start () {
		elapsedTime = 0.0f;
		inBurst = false;
	}

	void Update () {
		elapsedTime += Time.deltaTime;
		if (inBurst) {
			while (elapsedTime > burstShootingInterval && currentBurst < burstLength) {
				elapsedTime -= burstShootingInterval;
				currentBurst++;
				Instantiate (bullet, transform.position, Quaternion.identity);
			}
			if (currentBurst == burstLength) {
				inBurst = false;
			}
		} else {
			while (elapsedTime > intervalBetweenBursts) {
				elapsedTime -= intervalBetweenBursts;
				inBurst = true;
				currentBurst = 0;
			}
		}
	}
}
