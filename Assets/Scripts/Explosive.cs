﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour {

	private float elapsedTime;
	public float speed;
	public float explosionDelay;

	public GameObject bullet;
	public int bulletCount;

	private Vector3 direction;

	void Start () {
		elapsedTime = 0.0f;

		GameObject target = GameObject.Find ("Player");
		if (target != null) {
			direction = (target.transform.position - transform.position);
		} else {
			Destroy (this.gameObject);
		}
	}

	void Update () {
		transform.Translate (direction * Time.deltaTime * speed);

		elapsedTime += Time.deltaTime;
		if (elapsedTime > explosionDelay) {
			float angle = 0.0f;
			float angleIncrement = 360.0f / bulletCount;
			for (int i = 0; i < bulletCount; i++) {
				GameObject obj = Instantiate (bullet, transform.position, Quaternion.identity) as GameObject;
				obj.transform.Rotate (new Vector3 (0.0f, 0.0f, angle));
				angle += angleIncrement;
			}
			Destroy (this.gameObject);
		}
	}
}
