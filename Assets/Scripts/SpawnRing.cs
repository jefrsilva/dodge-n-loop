﻿using UnityEngine;
using System.Collections;

public class SpawnRing : MonoBehaviour {

	private GameObject enemyToSpawn;

	void Start () {
	
	}
	
	void Update () {
	
	}

	public void SetEnemyToSpawn(GameObject enemy) {
		this.enemyToSpawn = enemy;
	}

	public void Spawn() {
		Instantiate (enemyToSpawn, transform.position, Quaternion.identity);
		Destroy (this.gameObject);
	}
}
