﻿using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour {
	
	private Vector2 lastPosition;

	void Start () {

	}
	
	void Update () {
		Ray ray;
		RaycastHit hitInfo;
		if (Input.touchCount > 0) {
			Touch touch = Input.touches [0];
			switch (touch.phase) {
			case TouchPhase.Began:
				ray = Camera.main.ScreenPointToRay (touch.position);
				Physics.Raycast (ray, out hitInfo);

				lastPosition = hitInfo.point;
				break;

			case TouchPhase.Moved:
				ray = Camera.main.ScreenPointToRay (touch.position);
				Physics.Raycast (ray, out hitInfo);

				Vector2 currentPosition = hitInfo.point;
				Vector2 viewportDeltaPosition = currentPosition - lastPosition;

				transform.Translate (viewportDeltaPosition);

				if (transform.position.x < -0.55f) {
					transform.position = new Vector3(-0.55f, transform.position.y, transform.position.z);
				} else if (transform.position.x > 0.55f) {
					transform.position = new Vector3(0.55f, transform.position.y, transform.position.z);
				}

				if (transform.position.y < -0.9f) {
					transform.position = new Vector3(transform.position.x, -0.9f, transform.position.z);
				} else if (transform.position.y > 0.9f) {
					transform.position = new Vector3(transform.position.x, 0.9f, transform.position.z);
				}

				lastPosition = currentPosition;
				break;
			}
		}
	}
}
