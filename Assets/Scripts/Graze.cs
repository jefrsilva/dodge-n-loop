﻿using UnityEngine;
using System.Collections;

public class Graze : MonoBehaviour
{
	public GameObject grazeFX;
	private bool grazed;
	private Player player;

	void Start ()
	{
		GameObject obj = GameObject.FindGameObjectWithTag ("Player");
		if (obj != null) {
			player = obj.GetComponent<Player> ();
		}
		grazed = false;
	}

	void Update ()
	{
	
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Player") && !grazed) {
			grazed = true;
			Instantiate (grazeFX, transform.position, Quaternion.identity);
			player.notifyDodge ();
		}
	}
}
