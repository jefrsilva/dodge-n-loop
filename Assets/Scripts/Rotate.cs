﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public float angularSpeed;

	void Start () {
	
	}

	void Update () {
		transform.Rotate(new Vector3(0.0f, 0.0f, -angularSpeed * Time.deltaTime));	
	}
}
