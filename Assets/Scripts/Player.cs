﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

	private int dodges;
	private float loopTime;
	private GameController gameController;
	public GameObject deathFX;
	public GameObject loopingFX;
	public float loopDuration;
	private TrailRenderer trail;

	void Start ()
	{
		GameObject obj = GameObject.Find ("GameController");
		gameController = obj.GetComponent<GameController> ();
		dodges = 0;
		loopTime = 0.0f;
		trail = GetComponent<TrailRenderer> ();
		trail.time = 0.1f;
	}

	void Update ()
	{
		if (loopTime > 0.0f) {
			loopTime -= Time.deltaTime;			
			if (loopTime <= 0.0f) {
				trail.time = 0.1f;
				loopTime = 0.0f;
			}
				
		}
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag ("Bullet")) {
			gameController.EndGame ();
			Instantiate (deathFX, transform.position, Quaternion.identity);
			Destroy (this.gameObject);
		}
	}

	public void notifyDodge ()
	{
		gameController.addScore (100);
		gameController.notifyDodge ();
		if (loopTime <= 0.0f) {
			dodges++;		
			if (dodges >= 5) {
				Instantiate (loopingFX, transform.position, Quaternion.identity);
				loopTime = loopDuration;
				gameController.notifyLoop (loopTime);
				trail.time = 1.0f;
				dodges = 0;
			}
		}
	}

	public void notifyDestroy() {
		gameController.addScore (1500);
	}

	public bool isLooping ()
	{
		return loopTime > 0.0f;
	}
}
