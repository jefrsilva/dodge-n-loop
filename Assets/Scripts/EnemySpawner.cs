﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	private float elapsedTime;
	private float totalTime;
	private int level;

	public Vector2 spawnArea;

	[Range (0.1f, 30.0f)]
	public float spawnInterval;

	public GameObject spawnRing;
	public List<GameObject> enemies;

	void Start () {
		elapsedTime = 0.0f;
		totalTime = 0.0f;
		level = 1;
	}

	void Update () {
		totalTime += Time.deltaTime;
		while (totalTime >= 10.0f) {
			totalTime -= 10.0f;
			if (level < 10) {
				level++;
			} else {
				if (spawnInterval > 3.0f) {
					spawnInterval -= 0.1f;
				}
			}
		}

		elapsedTime += Time.deltaTime;
		while (elapsedTime > spawnInterval) {
			elapsedTime -= spawnInterval;
			Spawn ();
		}
	}

	void Spawn () {
		Vector2 spawnPosition = new Vector2 ((Random.value * spawnArea.x) - (spawnArea.x / 2.0f), 
			(Random.value * spawnArea.y) - (spawnArea.y / 2.0f)); 
		GameObject obj = Instantiate (spawnRing, spawnPosition, Quaternion.identity) as GameObject;
		SpawnRing ring = obj.GetComponent<SpawnRing> ();

		int enemy = Random.Range (0, level);

		ring.SetEnemyToSpawn (enemies[enemy]);
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = Color.yellow;
		Gizmos.DrawLine (new Vector3 (-spawnArea.x / 2.0f, -spawnArea.y / 2.0f, 0.0f),
			new Vector3 (spawnArea.x / 2.0f, -spawnArea.y / 2.0f, 0.0f));
		Gizmos.DrawLine (new Vector3 (-spawnArea.x / 2.0f, spawnArea.y / 2.0f, 0.0f),
			new Vector3 (spawnArea.x / 2.0f, spawnArea.y / 2.0f, 0.0f));
		
		Gizmos.DrawLine (new Vector3 (-spawnArea.x / 2.0f, -spawnArea.y / 2.0f, 0.0f),
			new Vector3 (-spawnArea.x / 2.0f, spawnArea.y / 2.0f, 0.0f));
		Gizmos.DrawLine (new Vector3 (spawnArea.x / 2.0f, -spawnArea.y / 2.0f, 0.0f),
			new Vector3 (spawnArea.x / 2.0f, spawnArea.y / 2.0f, 0.0f));
	}
}
