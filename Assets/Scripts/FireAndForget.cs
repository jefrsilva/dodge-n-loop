﻿using UnityEngine;
using System.Collections;

public class FireAndForget : MonoBehaviour {

	public float speed;

	void Start () {
		GameObject target = GameObject.Find ("Player");
		if (target != null) {
			transform.up = (target.transform.position - transform.position);
		} else {
			Destroy (this.gameObject);
		}
	}
	
	void Update () {
		transform.Translate (Vector3.up * speed * Time.deltaTime);			
	}
}
